# Horny Bot

Discord bot to help decide if and how horny you are. Please read the [Setup](#setup) 
section first.



## How to use

In order to start up the bot, you can just run the following command in the console:

```
python main.py
```



## Setup <a name="setup"></a>

Before you run any of the scripts you need to get a Discord-Bot Token. Instructions on how to do that can be found [here](https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/).
Create a file called "TOKEN.txt" and insert the obtained token. <mark>DO NOT ADD ANYTHING ELSE TO THE FILE!</mark> The 
token will be read automatically on startup


If you run the bot for the first time, the requirements should install themselves automatically.
If not, either run 

```
python installer.py
```

 or install them manually using the following command:

```
pip install -r requirements.txt
```
