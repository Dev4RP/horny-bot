try:
    import discord
    import horny
    from prettytable import PrettyTable
    import platform
    import os

except Exception as e:
    print("Installing requirements")
    import installer
    installer.install()


client = discord.Client()


@client.event
async def on_ready():
    logo1 = "  _    _                          ____        _   \n"
    logo2 = " | |  | |                        |  _ \      | |  \n"
    logo3 = " | |__| | ___  _ __ _ __  _   _  | |_) | ___ | |_ \n"
    logo4 = " |  __  |/ _ \| '__| '_ \| | | | |  _ < / _ \| __|\n"
    logo5 = " | |  | | (_) | |  | | | | |_| | | |_) | (_) | |_ \n"
    logo6 = " |_|  |_|\___/|_|  |_| |_|\__, | |____/ \___/ \__|\n"
    logo7 = "                           __/ |                  \n"
    logo8 = "                          |___/                   \n"

    print(logo1 + logo2 + logo3 + logo4 + logo5 + logo6 + logo7 + logo8)
    print('Horny Bot is now online as {0.user}'.format(client))
    print("To exit press ctrl + c")
    await client.change_presence(activity=discord.Game('with myself'))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    messageSend = message.content.split(' ')

    if any('get-horny' in s for s in messageSend):
        msg = str(horny.sex_o_meter())
        await message.channel.send(msg)

    if any('help-horny' in s for s in messageSend):
        # Generate Table for Horny Levels
        tHorny = PrettyTable(['Name', 'Value'])
        tHorny.add_row(['horny', '100'])
        tHorny.add_row(['somewhat horny', '75'])
        tHorny.add_row(['sex after christian marriage', '50'])
        tHorny.add_row(['christian', '25'])

        # Generate Table for Kinky Factor
        tKinky = PrettyTable(['Name', 'Value'])
        tKinky.add_row(['Ben Shapiro humiliation', '0'])
        tKinky.add_row(['feetjob CBT', '25'])
        tKinky.add_row(['no condom', '50'])
        tKinky.add_row(['missionary only', '75'])

        # Generate Table for Hornyness Level
        tHornyness = PrettyTable(['Name', 'Value'])
        tHornyness.add_row(['le biggest horny', '100'])
        tHornyness.add_row(['dick already in hand', '75'])
        tHornyness.add_row(['hornier then normal', '50'])
        tHornyness.add_row(['normal amount', '25'])
        tHornyness.add_row(['good christian', '0'])
        tHornyness.add_row(['buddhist monk', 'smaller than 0'])

        # Assemble final message
        msg = "To get the horny level type: ```get-horny```" + "\n" + \
              "Horny Level:\n" + "```\n" + str(tHorny) + "```" \
              "\nKinky Factors:\n" + "```\n" + str(tKinky) + "```" + \
              "\nFinal Hornyness:" + "```\n" + str(tHornyness) + "```" + \
              "\nFormula used to calculate the final hornyness: ```HornyLevel - KinkyFactor```" + \
              "\n\nThe Horny Bot is based on Limpowraiths Horny Calculator." + \
              "\nFor questions write a message to the following E-Mail:\nfuck@off.zoey"

        # Sends Message
        await message.channel.send(msg)


def clearConsole():
    if platform.system() == "Windows":
        os.system('cls')
    elif platform.system() == "Linux":
        os.system('clear')


if __name__ == '__main__':
    clearConsole()
    token = open("TOKEN.txt", "r")
    client.run(str(token.read()))
