import subprocess
import sys


def install():
    print("\nInstalling Python packages form requirements.txt")
    subprocess.check_call([sys.executable, "-m", "pip", "install", "-r", "requirements.txt"])


if __name__ == '__main__':
    install()
